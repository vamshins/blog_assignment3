class Comment < ActiveRecord::Base
	belongs_to :post
  	validates :post, presence: true
  	validates :body, presence: true

  	scope :sorted, lambda { order("comments.id ASC")}
    scope :newest_first, lambda { order("comments.created_at DESC")}
end
