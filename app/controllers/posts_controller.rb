class PostsController < ApplicationController
  # layout "admin"

  def index
    @posts = Post.sorted
  end

  def show
    @post = Post.find(params[:id])
    @comments = Comment.where(["post_id = ?", @post.id])
  end

  def new
    @post = Post.new
  end

  def create
    # Instantiate a new object using form parameters
    @post = Post.new(post_params)
    # Save the object
    if @post.save
      # If save succeeds, redirect to the index action
      flash[:notice] = "Post created successfully."
      redirect_to(:action => 'index')
    else
      # If save fails, redisplay the form so user can fix problems
      render('new')
    end
  end

  def edit
    @post = Post.find(params[:id])
  end

  def update
    # Find an existing object using form parameters
    @post = Post.find(params[:id])
    # Update the object
    if @post.update_attributes(post_params)
      # If save succeeds, redirect to the index action
      flash[:notice] = "Post updated successfully."
      redirect_to(:action => 'show', :id => @post.id)
    else
      # If save fails, redisplay the form so user can fix problems
      render('edit')
    end
  end

  def delete
    puts "delete called"
    @post = Post.find(params[:id])
  end

  def destroy
    post = Post.find(params[:id]).destroy
    flash[:notice] = "Post '#{post.title}' destroyed successfully."
    redirect_to(:action => 'index')
  end

  # Writing Comment Create (Add Comment Button) functionality here
  def createComment
    puts "Creating comment"
    # Instantiate a new object using form parameters
    @comment = Comment.new(comment_params)
    # Save the object
    if @comment.save
      # If save succeeds, redirect to the index action
      flash[:notice] = "Comment created successfully."
      redirect_to(:action => 'show', :id => @comment.post_id)
    else
      # If save fails, redisplay the form so user can fix problems
      flash[:notice] = "Comment is not created. Please try again."
      redirect_to(:action => 'show', :id => @comment.post_id, :errors => @comment.errors)
    end
  end

  def editComment
  end

  def deleteComment
  end

  private
  def post_params
    # same as using "params[:post]", except that it:
    # - raises an error if :post is not present
    # - allows listed attributes to be mass-assigned
    params.require(:post).permit(:title, :body)
  end

  def comment_params
    # same as using "params[:comment]", except that it:
    # - raises an error if :comment is not present
    # - allows listed attributes to be mass-assigned
    params.require(:comment).permit(:post_id, :body)
  end
end
