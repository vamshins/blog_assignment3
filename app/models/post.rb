class Post < ActiveRecord::Base

	has_many :comments, dependent: :destroy
	validates :title, presence: true
#    validates :title, presence: {message: "Title is not valid, post is not saved"}
    validates :body, presence: true
#    validates :body, presence: {message: "Body is not valid, post is not saved"}
    
    scope :sorted, lambda { order("posts.id ASC")}
    scope :newest_first, lambda { order("posts.created_at DESC")}
end
