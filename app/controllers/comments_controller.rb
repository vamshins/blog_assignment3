class CommentsController < ApplicationController
  layout "admin"

  def index
    @comments = Comment.sorted
  end

  def show
    @comment = Comment.find(params[:id])
  end

  def new
    @comment = Comment.new
  end

  def create
    # Instantiate a new object using form parameters
    @comment = Comment.new(comment_params)
    # Save the object
    if @comment.save
      # If save succeeds, redirect to the index action
      flash[:notice] = "Comment created successfully."
      redirect_to(:action => 'index')
    else
      # If save fails, redisplay the form so user can fix problems
      render('new')
    end
  end

  def edit
    @comment = Comment.find(params[:id])
  end

  def update
    # Find an existing object using form parameters
    @comment = Comment.find(params[:id])
    # Update the object
    if @comment.update_attributes(comment_params)
      # If save succeeds, redirect to the index action
      flash[:notice] = "Comment updated successfully."
      redirect_to(:controller => 'posts', :action => 'show', :id => @comment.post_id)
    else
      # If save fails, redisplay the form so user can fix problems
      flash[:notice] = "Comment not updated. Please try again"
      redirect_to(:controller => 'posts', :action => 'show', :id => @comment.post_id)
    end
  end

  def delete
    @comment = Comment.find(params[:id])
  end

  def destroy
    comment = Comment.find(params[:id])
    post_id = comment.post_id
    comment.destroy
    flash[:notice] = "Comment '#{comment.body}' destroyed successfully."
    redirect_to(:controller => 'posts', :action => 'show', :id => post_id)
  end

  private
  def comment_params
    # same as using "params[:comment]", except that it:
    # - raises an error if :comment is not present
    # - allows listed attributes to be mass-assigned
    params.require(:comment).permit(:post_id, :body)
  end
end
